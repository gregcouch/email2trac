= email2trac - utilities

This is a release of the SURFsara package email2trac that contains utilities that 
we use to convert emails to trac tickets. The initial setup was made by 
Daniel Lundin from Edgewall Software. SURFsara has extend the initial setup,  
with the following extensions:

  * HTML message conversion
  * Attachments
  * Tickets can be updated via email
  * Use command-line options
  * Configuration file to control the behavior.
  * Unicode support
  * SPAM detection
  * Workflow support
  * FullBlogPlugin support
  * DiscussionPlugin support 

See [INSTALL](https://gitlab.com/surfsara/email2trac/-/wikis/Installation) for the how to setup the utilities

**NOTE:** The master branch has been ported to python3 and is work in progress. The 2.14.0 is the lastest stable python2 code.


Comments or Suggestions mail them to: 
  * [Creat an issue](https://gitlab.com/surfsara/email2trac/-/issues/new)
  
http://www.surf.nl for more info about SURF.


